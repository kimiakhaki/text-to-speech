import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule, Routes} from "@angular/router";
import {HomePageeComponent} from "./Components/home-pagee/home-pagee.component";
import {HistoryComponent} from "./Components/history/history.component";
import {TextToSpeechComponent} from "./Components/text-to-speech/text-to-speech.component";


const routes: Routes = [
  { path: '', component: HomePageeComponent,
    children: [
      { path: 'history', component: HistoryComponent},
      { path: '', component: TextToSpeechComponent},
    ]
}
];
@NgModule({
  declarations: [],
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
