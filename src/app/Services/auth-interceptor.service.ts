import { Injectable } from '@angular/core';
import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from "@angular/common/http";
import {Observable} from "rxjs";
import {environment} from "../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class AuthInterceptorService implements HttpInterceptor {

  constructor() { }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    let bAuth = btoa('apiKey:'+ environment.apiKey);
    const req1 = req.clone({
      headers: req.headers.set('Authorization', `Basic ${bAuth}`),
    });

    return next.handle(req1);
  }
}
