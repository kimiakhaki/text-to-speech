import { Injectable } from '@angular/core';
import {HttpClient, HttpErrorResponse} from "@angular/common/http";
import {EMPTY, Observable} from "rxjs";
import {catchError, map, shareReplay} from "rxjs/operators";
import {environment} from "../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class ConvertToSpeechService {
  apiUrl: string = environment.apiUrl;
  cors: string = environment.corsEverywhereServer;
  cache: {} [] = [];
  constructor(private http: HttpClient) {
  }

  getAudioFile (text: string): Observable<Blob> {
    if (localStorage.getItem(text)) {
       return new Observable<Blob>(() => {
         new Blob([localStorage.getItem(text)], {type: 'audio/wav'});
       })
    }
    return this.http.post(this.cors + this.apiUrl, {}, {
        headers: {
          'Content-Type' : 'application/json',
          'Accept' : 'audio/wav',
        },
        params: {
          voice: 'en-US_AllisonV3Voice',
          text : text
        },
        responseType: 'blob'
        // responseType: 'arraybuffer'
      }).pipe(map(res => {
        res.text().then((strBlob => {
          try {
            localStorage.setItem(text, strBlob);
          } catch (e) {
            console.log(e)
          }
        }))
      // this.blobToBase64(res, text).then((b64 => {
      // }));
        return res;
      }),catchError(this.handleError))
  }

  private handleError(res: HttpErrorResponse) {
    console.error(res.error);
    return Observable.throw(res.error || 'Server error');
  }
  // blobToBase64(blob, txt) {
  //   return new Promise((resolve, rej) => {
  //     const reader = new FileReader();
  //     reader.onload = (e) => {
  //       // @ts-ignore
  //       localStorage.setItem(txt, e.target.result);
  //       resolve(reader.result);
  //     }
  //     reader.readAsBinaryString(blob)
  //   })
  // }
}


