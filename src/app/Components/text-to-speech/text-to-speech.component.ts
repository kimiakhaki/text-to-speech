import {Component, OnInit} from '@angular/core';
import {ConvertToSpeechService} from "../../Services/convert-to-speech.service";
import {NgxSpinnerService} from "ngx-spinner";


@Component({
  selector: 'app-text-to-speech',
  templateUrl: './text-to-speech.component.html',
  styleUrls: ['./text-to-speech.component.css']
})
export class TextToSpeechComponent implements OnInit {
  text: string = '';
  textIsEmptyOnSubmit: boolean = false;
  loading: boolean = false;

  constructor(private toSpeechService: ConvertToSpeechService) {
  }

  ngOnInit(): void {
  }

  toSpeech() {
    this.textIsEmptyOnSubmit = false;
    if (this.text.length) {
      this.loading = true;
        this.toSpeechService.getAudioFile(this.text).subscribe(res => {
          console.log(res)
          // debugger;
          this.loading = false;
          const EL_audio = document.querySelector("audio");
          // @ts-ignore
          EL_audio.src = URL.createObjectURL(res);
          // @ts-ignore
          EL_audio.load();

          // const context = new AudioContext();
          // let source = context.createBufferSource();
          // context.decodeAudioData(res, function (buffer) {
          //   source.buffer = buffer;
          // }, null).then(() => {
          //   source.connect(context.destination);
          //   source.start(0);
          //   // this.audioSrc = source;
          // });
        });

      // });

    } else {
      this.textIsEmptyOnSubmit = true;
    }

  }
}
