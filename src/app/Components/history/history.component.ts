import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-history',
  templateUrl: './history.component.html',
  styleUrls: ['./history.component.css']
})
export class HistoryComponent implements OnInit {
  historyObjectList: {
    text: string;
    file: string;
  }[] = [];

  constructor() { }

  ngOnInit(): void {
    this.getHistoryFromLocalStorage();
  }
  getHistoryFromLocalStorage() {
    this.historyObjectList = [];
    for (let i = 0; i < localStorage.length; i++) {
      const key = localStorage.key(i);
      const value = localStorage.getItem(key);
      let obj = {};
      obj[key] = value
      this.historyObjectList.push({
        text: key,
        file: value
      })
    }
  }

  playSpeech(file: string) {
    // var source = audioCtx.createBufferSource();
    // var audioBuffer = audioCtx.createBuffer(1, sampleArray.length, audioCtx.sampleRate);
    // audioBuffer.copyToChannel(sampleArray);
    // source.buffer = audioBuffer;
    // source.connect(audioCtx.destination);
    // source.start();
   const blob = new Blob([file], {type : 'audio/wav'})
    // new Response(blob).text().then(res => {
    //   const audio = new Audio(URL.createObjectURL(blob));
    //   audio.play().catch(console.log);
    // });

    const context = new AudioContext();
    let source = context.createBufferSource();
    blob.arrayBuffer().then(bufferArray => {
      console.log(bufferArray)
      context.decodeAudioData(bufferArray, function (buffer) {
        source.buffer = buffer;

      }, function (err) {
        console.log(err)
      }).then(() => {
        source.connect(context.destination);
        source.start(0);
      });
    })
  }

  removeSpeech(text: string) {
    localStorage.removeItem(text);
    this.getHistoryFromLocalStorage();
  }
}
