import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { TextToSpeechComponent } from './Components/text-to-speech/text-to-speech.component';
import { HomePageeComponent } from './Components/home-pagee/home-pagee.component';
import { FooterComponent } from './Components/footer/footer.component';
import { NavbarComponent } from './Components/navbar/navbar.component';
import { HistoryComponent } from './Components/history/history.component';
import {HTTP_INTERCEPTORS, HttpClientModule} from "@angular/common/http";
import {FormsModule} from "@angular/forms";
import {AuthInterceptorService} from "./Services/auth-interceptor.service";

@NgModule({
  declarations: [
    AppComponent,
    TextToSpeechComponent,
    HomePageeComponent,
    FooterComponent,
    NavbarComponent,
    HistoryComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptorService,
      multi: true },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
